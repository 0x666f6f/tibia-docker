#!/bin/bash
# Script to build a Docker image to run the Tibia 11 Linux Client.
# 
# This script is necessary because:
# > The client requires an old version of OpenSSL that modern distributions no longer use. Docker containers are used to resolve this problem without having to downgrade your system version.
# > The client saves it's files in multiple locations. This script sets up Docker volumes to manage these requirements (and keep the host filesystem unchanged).
#
# The base image used to build the target image is a tiny Ubuntu 18.04 distribution with OpenSSL preinstalled (only 90MB). See: https://hub.docker.com/r/shamelesscookie/openssl
#
# The image that this script builds can be executed with:
# docker run --rm --privileged --name tibia-client -e "DISPLAY=$DISPLAY" -v "/tmp/.X11-unix:/tmp/.X11-unix:rw" --mount source="tibia-client-volume",target="/usr/local/bin" --mount source="tibia-settings-volume",target="/home/tibia_player/.local/share" tibia-docker

set -euo pipefail
IFS=$'\n\t'

# Start by downloading and extracting the client, if it doesn't already exist in this directory.
[ ! -f "./tibia.x64.tar.gz" ] && wget https://static.tibia.com/download/tibia.x64.tar.gz || :
tar xf ./tibia.x64.tar.gz
rm ./tibia.x64.tar.gz

# Create a Docker volume to store the client files. (Mounted at /usr/local/bin)
docker volume rm tibia-client-volume || :  # Delete the volume if it already exists.
docker container create --name temp-container -v tibia-client-volume:/data busybox
docker cp ./Tibia temp-container:/data
docker rm temp-container
rm -rf ./Tibia

# Confirm the launcher exists in the volume.
docker run --rm --name tibia-client --mount source="tibia-client-volume",target="/usr/local/bin" --entrypoint '/bin/sh' busybox -c "ls -l /usr/local/bin/Tibia/start-tibia-launcher.sh"

# Create another Docker volume to store the map files and other downloaded content. (Mounted at ~/.local/share)
docker volume rm tibia-settings-volume || :  # Delete the volume if it already exists.
docker volume create tibia-settings-volume

# Build the Docker image for running the client.
docker build --no-cache -t tibia-docker .

# Done!
