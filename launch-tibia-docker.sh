#!/bin/bash launch-tibia-docker.sh
# Script to run a Docker container hosting the Tibia 11 Linux Client.
#
# This script expects the "tibia-docker:latest" image to be available. This image is created with the "build-image.sh" script, so run that once first! Thereafter, just use this script to launch Tibia.

docker run \
  --rm \
  --privileged \
  --name tibia-client \
  -e "DISPLAY=$DISPLAY" \
  -v "/tmp/.X11-unix:/tmp/.X11-unix:rw" \
  --mount source="tibia-client-volume",target="/usr/local/bin" \
  --mount source="tibia-settings-volume",target="/home/tibia_player/.local/share" \
  tibia-docker:latest
