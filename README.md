# Tibia Docker

**2019-07-25**

> A Docker image to run the Tibia Linux Client 11 on modern Linux distributions with newer (incompatible) versions of OpenSSL.  

### Background   
The Tibia Linux Client v11 requires OpenSSL 1.0.x, but most modern Linux distributions use 1.1.x or higher. Rather than downgrading my OpenSSL installation, I decided to just run the Tibia Client in a Docker container instead. I set this up for my own purposes, but I'm releasing it publicly in case someone else comes across the same challenges and wants to use this solution.  

The image, plus associated build and run scripts, are designed for anyone to use, provided that they have Docker installed. The Docker version I developed this on was 18.09.7, build 2d0083d.  

In case you're wondering (like I did) whether you needed nvidia-docker(-|1|2) installed, it turns out that you don't! The key to overcoming OpenGL passthrough problems was to include `--privileged`, `-e "DISPLAY=$DISPLAY"` and `-v "/tmp/.X11-unix:/tmp/.X11-unix:rw"` options as part of the `docker run` command. See: [launch-tibia-docker.sh](launch-tibia-docker.sh).  

### Instructions  

1: Clone the repository  
- `cd ~/Downloads`  
- `git clone git@bitbucket.org:0x666f6f/tibia-docker.git`  

2: If the scripts (`build-image.sh` and `launch-tibia-docker.sh`) aren't executable, make them so with `sudo chmod + x ./tibia-docker/*.sh`.

3: Build the image.  
- `bash ./tibia-docker/build-image.sh`  
- When the script is finished, a new Docker image will be visible with the `docker images` command: *tibia-docker:latest*.  
  This step will also download tibia.x64.tar.gz from the CipSoft servers if it doesn't already exist. I've included the version that I downloaded from there in this repo, but you can just as easily delete that and let the build script download a fresh copy.  


4: Run it!  
`bash ./tibia-docker/launch-tibia-docker.sh`
